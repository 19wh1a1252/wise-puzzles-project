print("5*5 SLIDE PUZZLE GAME")
print("Type A for sliding up")
print("Type B for sliding down")
print("Type R for sliding right")
print("Type L for sliding left\n")
print("puzzle Input:")

a = []
s = []
for i in range(5):
    e = input()
    s = []
    for j in e:
        s.append(j)
    a.append(s)


def grid():
    global r, c
    for i in range(5):
        for j in range(5):
            print(a[i][j], end=' ')
            if a[i][j] == ' ':
                r = i
                c = j
        print()
    print()


grid()


def check():
    global r
    if (r < 6):
        return True
    else:
        print("Sorry you can't solve this puzzle")


def right(a):
    global c
    if c != 0:
        t = a[r][c - 1]
        a[r][c - 1] = ' '
        a[r][c] = t


def left(a):
    global c
    if c != 4:
        t = a[r][c + 1]
        a[r][c + 1] = ' '
        a[r][c] = t


def above(a):
    global r
    if r != 4:
        t = a[r + 1][c]
        a[r + 1][c] = ' '
        a[r][c] = t


def below(a):
    global r
    if r != 0:
        t = a[r - 1][c]
        a[r - 1][c] = ' '
        a[r][c] = t


while check():
    m = input()
    if m == 'L':
        right(a)
        grid()
    elif m == 'R':
        left(a)
        grid()
    elif m == 'B':
        above(a)
        grid()
    elif m == 'A':
        below(a)
        grid()
    elif m == '0':
        grid()
        continue
    elif m == 'Z':
        print("No final Configuration")
        exit()
