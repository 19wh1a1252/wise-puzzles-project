def find(a):
    l = []
    for i in range(0, 5):
        if (' ' in a[i]):
            l.append(i)
            l.append(a[i].index(' '))
            break
    return l
def check(r, c):
    return (r >= 0 and c >= 0 and r < 5 and c < 5)

def grid(a):
    for i in a:
        print(*i)
    print()

a = []
row = 0
col = 0
while (1):
    a = []
    b = True
    fl = input()
    if (fl == "Z"):
        break
    else:
        fl = list(fl)
        if (len(fl) == 5):
            a.append(fl)
            if (' ' in fl):
                row = 0
                col = fl.index(' ')
        else:
            fl.append(' ')
            a.append(fl)
            if (' ' in fl):
                row = 0
                col = fl.index(' ')
        for i in range(1, 5):
            e = list(input())
            if (len(e) == 4):
                e.append(' ')
            a.append(e)
            if (' ' in e):
                row = i
                col = e.index(' ')
        m = ""
        while (1):
            t = input()
            if (t[-1] == '0'):
                m += t
                break
            m += t
        for j in range(0, len(m) - 1):
            if (m[j] == 'L'):
                if (check(row, col - 1)):
                    a[row][col], a[row][col - 1] = a[row][col - 1], a[row][col]
                    tl = find(a)
                    row = tl[0]
                    col = tl[1]
                else:
                    b = False
                    print("This Puzzle has no Final Configuration")
                    break
            elif (m[j] == 'R'):
                if (check(row, col + 1)):
                    a[row][col], a[row][col + 1] = a[row][col + 1], a[row][col]
                    tl = find(a)
                    row = tl[0]
                    col = tl[1]
                else:
                    b = False
                    print("This Puzzle has no Final Configuration")
                    break
            elif (m[j] == 'B'):
                if (check(row + 1, col)):
                    a[row][col], a[row + 1][col] = a[row + 1][col], a[row][col]
                    tl = find(a)
                    row = tl[0]
                    col = tl[1]
                else:
                    b = False
                    print("This Puzzle has no Final Configuration")
                    break
            elif (m[j] == 'A'):
                if (check(row - 1, col)):
                    a[row][col], a[row - 1][col] = a[row - 1][col], a[row][col]
                    tl = find(a)
                    row = tl[0]
                    col = tl[1]

                else:
                    b = False
                    print("This Puzzle has no Final Configuration")

                    break
        print()
        if (b):
            grid(a)