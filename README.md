_**PROJECT NAME:**_ PUZZLE

_**TEAM MEMBERS:**_
- GUJJARLAPUDI SPANDANA: 19WH1A1252 | IT
- POOSALA PREETHI:       19WH1A1231 | IT
- B. LAKSHMI TEJASWINI:  19WH1A0206 | EEE
- CH JENNIFER:           19WH1A0462 | ECE
- MITTAPELLY ARADYA:     19WH1A0525 | CSE
- SUKHAVSI NIKITHA:      19WH1A0516 | CSE
- MELLACHERVU SRIHITHA:  20WH5A0510 | CSE

**_PROJECT  DESCRIPTION:_**

A children’s puzzle that was popular 30 years ago consisted of a 5×5 frame which contained 24 small squares of equal size. A unique letter of the alphabet was printed on each small square. Since there were only 24 squares within the frame, the frame also contained an empty position which was the same size as a small square. A square could be moved into that empty position if it were immediately to the right, to the left, above, or below the empty position. The object of the puzzle was to slide squares into the empty position so that the frame displayed the letters in alphabetical order.

The illustration below represents a puzzle in its original configuration and in its configuration after the following sequence of 6 moves:

The square above the empty position moves.
The square to the right of the empty position moves.
The square to the right of the empty position moves.
The Square below the empty Position moves.
The square below the empty Position moves.
The square to the left of the empty position moves.

Example

Input:                      


- TRGSJ                      
- XDOKI                      
- M" "VLN                      
- WPABE                      
- UQHCF                      
- ARRBBL0


 Output:


- T R G S J
- X O K L I
- M D V B N
- W P "  "  A E
- U Q H C F


**_TechStack_**

- Python    - Coding Language.
- PyCharm   - Coding Platform.
- GitLab    - Version Control.
